<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">
<link href="<?php echo URLROOT; ?>/img/favicon.png" rel="icon" />
<title><?php echo SITENAME; ?></title>

<!-- Web Fonts
============================================= -->
<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Rubik:300,300i,400,400i,500,500i,700,700i,900,900i' type='text/css'>

<!-- Stylesheet
============================================= -->
<link rel="stylesheet" type="text/css" href="<?php echo URLROOT; ?>/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo URLROOT; ?>/css/font-awesome/css/all.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo URLROOT; ?>/css/owl.carousel.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo URLROOT; ?>/css/stylesheet.css" />
</head>
<body>