<?php require APPROOT . '/Views/inc/header.php'; ?>
<!-- Preloader -->
<div id="preloader">
  <div data-loader="dual-ring"></div>
</div>
<!-- Preloader End --> 

<!-- Document Wrapper   
============================================= -->
<div id="main-wrapper">
  
  <?php require APPROOT . '/Views/inc/nav.php'; ?>

  <!-- Page Header
============================================= -->
<section class="page-header page-header-text-light bg-dark-3 py-5">
  <div class="container">
    <div class="row text-center">
      <div class="col-12">
        <ul class="breadcrumb mb-0">
          <li><a href="<?php echo URLROOT; ?>">Home</a></li>
          <li class="active">Contact Us</li>
        </ul>
      </div>
      <div class="col-12">
        <h1>Contact Us</h1>
      </div>
    </div>
  </div>
</section>
<!-- Page Header End --> 
  
  <!-- Content
  ============================================= -->
  <div id="content">
  <div class="container">
      <div class="row">
      
      <div class="col-md-4 mb-4">
      <div class="bg-white shadow-md rounded h-100 p-3">
        <div class="featured-box text-center">
              <div class="featured-box-icon text-primary mt-4"> <i class="fas fa-map-marker-alt"></i></div>
              <h3>Payyed Inc.</h3>
              <p>4th Floor, Plot No.22, Above Public Park<br>
                145 Murphy Canyon Rd.<br>
                Suite 100-18<br>
                San Diego CA 2028 </p>
            </div>
      </div>
      </div>
      
      <div class="col-md-4 mb-4">
      <div class="bg-white shadow-md rounded h-100 p-3">
        <div class="featured-box text-center">
              <div class="featured-box-icon text-primary mt-4"> <i class="fas fa-phone"></i> </div>
              <h3>Telephone</h3>
              <p class="mb-0">(+060) 9898980098</p>
              <p>(+060) 8898880088</p>
            </div>
      </div>
      </div>
      
      <div class="col-md-4 mb-4">
      <div class="bg-white shadow-md rounded h-100 p-3">
        <div class="featured-box text-center">
              <div class="featured-box-icon text-primary mt-4"> <i class="fas fa-envelope"></i> </div>
              <h3>Business Inquiries</h3>
              <p>info@payyed.com</p>
            </div>
      </div>
      </div>
      
      
      <div class="col-12 mb-4">
      <div class="text-center py-5 px-2">
      <h2 class="text-8">Get in touch</h2>
      <p class="lead">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
        <div class="d-flex flex-column">
              <ul class="social-icons social-icons-lg social-icons-colored justify-content-center">
                <li class="social-icons-facebook"><a data-toggle="tooltip" href="http://www.facebook.com/" target="_blank" title="" data-original-title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
                <li class="social-icons-twitter"><a data-toggle="tooltip" href="http://www.twitter.com/" target="_blank" title="" data-original-title="Twitter"><i class="fab fa-twitter"></i></a></li>
                <li class="social-icons-google"><a data-toggle="tooltip" href="http://www.google.com/" target="_blank" title="" data-original-title="Whatsapp"><i class="fab fa-whatsapp"></i></a></li>
                <li class="social-icons-youtube"><a data-toggle="tooltip" href="http://www.youtube.com/" target="_blank" title="" data-original-title="Telegrame"><i class="fab fa-telegram"></i></a></li>
              </ul>
            </div>
      </div>
      </div>
      
      </div>
    </div>

  <section class="hero-wrap section shadow-md">
      <div class="hero-mask opacity-9 bg-primary"></div>
      <div class="hero-bg" style="background-image:url('<?php echo URLROOT ?>/img/bg/naira.jpg');"></div>
      <div class="hero-content">
        <div class="container text-center">
          <h2 class="text-9 text-white">Awesome Customer Support</h2>
          <p class="text-4 text-white mb-4">Have you any query? Don't worry. We have great people ready to help you whenever you need it.</p>
          <!-- <a href="#" class="btn btn-light">Find out more</a> --> </div>
      </div>
    </section>
  <!-- Content end -->
  
  <!-- Footer
  ============================================= -->
  <footer id="footer">
    <div class="container">
      <div class="row">
        <div class="col-lg d-lg-flex align-items-center">
          <ul class="nav justify-content-center justify-content-lg-start text-3">
            <li class="nav-item"> <a class="nav-link" href="<?php echo URLROOT; ?> ">Home</a></li>
            <li class="nav-item"> <a class="nav-link" href="<?php echo URLROOT ?>/pages/about">About Us</a></li>
            <li class="nav-item"> <a class="nav-link" href="<?php echo URLROOT ?>/pages/FAQ">FAQ</a></li>
            <li class="nav-item"> <a class="nav-link" href="<?php echo URLROOT ?>/pages/contact">Contact</a></li>
          </ul>
        </div>
        <div class="col-lg d-lg-flex justify-content-lg-end mt-3 mt-lg-0">
          <ul class="social-icons justify-content-center">
            <li class="social-icons-facebook"><a data-toggle="tooltip" href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
            <li class="social-icons-twitter"><a data-toggle="tooltip" href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fab fa-twitter"></i></a></li>
            <li class="social-icons-google"><a data-toggle="tooltip" href="http://www.google.com/" target="_blank" title="Google"><i class="fab fa-whatsapp"></i></a></li>
            <li class="social-icons-youtube"><a data-toggle="tooltip" href="http://www.youtube.com/" target="_blank" title="Youtube"><i class="fab fa-telegram"></i></a></li>
          </ul>
        </div>
      </div>
      <div class="footer-copyright pt-3 pt-lg-2 mt-2">
        <div class="row">
          <div class="col-lg">
            <p class="text-center text-lg-left mb-2 mb-lg-0">Copyright &copy; 2020 <a
                href="https://www.bankalat.com">Bank Alat</a>. All Rights Reserved. <span class="text-success">Build <?php echo VERSION; ?></span>
              </p>
          </div>
          <div class="col-lg d-lg-flex align-items-center justify-content-lg-end">
            <ul class="nav justify-content-center">
              <li class="nav-item"> <a class="nav-link" href="<?php echo URLROOT; ?>/pages/privacy">Terms</a></li>
              <li class="nav-item"> <a class="nav-link" href="<?php echo URLROOT; ?>/pages/privacy">Privacy</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!-- Footer end --> 
  
</div>
<!-- Document Wrapper end --> 

<!-- Back to Top
============================================= --> 
<a id="back-to-top" data-toggle="tooltip" title="Back to Top" href="javascript:void(0)"><i class="fa fa-chevron-up"></i></a> 

<!-- Video Modal
============================================= -->
<div class="modal fade" id="videoModal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content bg-transparent border-0">
      <button type="button" class="close text-white opacity-10 ml-auto mr-n3 font-weight-400" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <div class="modal-body p-0">
        <div class="embed-responsive embed-responsive-16by9">
          <iframe class="embed-responsive-item" id="video" allow="autoplay"></iframe>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Video Modal end --> 
<?php require APPROOT . '/Views/inc/footer.php'; ?>
