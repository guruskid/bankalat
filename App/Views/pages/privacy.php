<?php require APPROOT . '/Views/inc/header.php'; ?>
<!-- Preloader -->
<div id="preloader">
  <div data-loader="dual-ring"></div>
</div>
<!-- Preloader End --> 

<!-- Document Wrapper   
============================================= -->
<div id="main-wrapper">
  
  <?php require APPROOT . '/Views/inc/nav.php'; ?>
  
  <!-- Content
  ============================================= -->
  <div id="content"> 
    
    <!-- Why choose us
    ============================================= -->
    <section class="section bg-white">
      <div class="container">
        <h2 class="text-9 text-center text-uppercase font-weight-400">Privacy</h2>
        <p class="text-4 text-center font-weight-300 mb-5">Bank Alat Privacy, Terms and Conditions</p>
        <div class="row">
          <div class="col-lg-10 mx-auto">
            <p class="text-justify text-4 font-weight-300 mb-5">By accessing and/or using this website, using the provided facilities and/or investing, Users are entering into a binding agreement with the Company and hereby agree to be bound by these Terms and Conditions and any additional Terms and Conditions, guidelines, restrictions, or rules posted on this website or any other related websites and applicable for any other services.</p>
            <p class="text-justify text-4 font-weight-300 mb-5">Bank Alat hereby reserves the right to make changes to this website and/or Terms and Conditions at any time without prior notice and in lines with what the management of Bank Alat sees serves the interest of the Company and her members.</p>
            <p class="text-justify text-4 font-weight-300 mb-5">By using this website, the Bank Alat Members hereby confirm that they have the possibility and right to review these Terms and Conditions each time they access this website, and thereby also to be bound by them.</p>
            <p class="text-justify text-4 font-weight-300 mb-5">By using this website, Bank Alat Members hereby confirm that they understand that Bank Alat admins will never chat them up on any of her social media platforms that they are merged to pay money to any account that will not reflect on your dashboard. This means that you must only make payments to the account details given to you on your dashboard. Members are equally aware of the existence of telegram, Instagram, Whatsapp, and other social media scammers and how they operate and that they will not fall prey to them but that if they fall to scam that they indemnify Bank Alat or any of her associate(s) from blame and that Bank Alat members must bear the full brunt of their actions.</p>
            <p class="text-justify text-4 font-weight-300 mb-5">In case of non-acceptance of the present Terms and Conditions the Company kindly asks visitors to not use this website.</p>
            <p class="text-justify text-4 font-weight-300 mb-5">All rights on all logotypes, images, trademarks, informational and other resources, or any other materials are reserved and therefore protected by the relevant legislation. Non-authorized use of any of the described is considered and shall be treated as a violation of the stipulated legislation and liability shall be seek in compliance with the relevant and applicable legislation.</p>
            <p class="text-justify text-4 font-weight-300 mb-5">Additionally, User preferences and settings (time zone, language, privacy preferences, product preferences, etc.) may be collected to enhance the performance of the present website.</p>
            <p class="text-justify text-4 font-weight-300 mb-5">Additionally, User preferences and settings (time zone, language, privacy preferences, product preferences, etc.) may be collected to enhance the performance of the present website.</p>
            <p class="text-justify text-4 font-weight-300 mb-5">By using this website, Members hereby agrees to not hold Bank Alat or any of her promoters responsible for any loss and/or damage that may occur as a result of delay in the process of executing a transaction or when participating in any of her programme. </p>
            <p class="text-justify text-4 font-weight-300 mb-5">By accessing and/or using this website, Users are entering into a binding agreement with the platform that they are aware and agree that the system is/will-be using the principle of first come first serve in assigning participants either to pay or receive and they agree and are aware that sometimes as a result of the queue it may take longer than they expected in being assigned a participant to pay or receive from and that they agree to hold Bank Alat or any of her promoters free from blame in the event that such delay leads to their loss of fund(s) and or any other loss that may arise as a result of such delay.</p>
            <p class="text-justify text-4 font-weight-300 mb-5">As is standard practice on many websites, the platform also logs non-personally-identifiable information including IP address, profile information, aggregate User data, and browser type, from Users and other visitors to the site or application. This data is used to improve the Services, to analyze trends, to administer the Services, to track Users’ movements around the Service and to gather demographic information about our User base as a whole. Bank Alat will not use the information collected to market directly to that person. This non-personally-identifiable information may be shared with third parties to provide more relevant services and special offers to Users. User IP addresses are recorded for security and monitoring purposes.</p>
            <p class="text-justify text-4 font-weight-300 mb-5">In addition, the platform and third parties may use pixel tags — tiny graphic images — to tell us what parts of our website Users have visited or to measure the effectiveness of searches Users perform on our site, among other data. Pixel tags also enable us to send email messages in formats Users can read. And they tell us whether emails have been opened to ensure that we’re only sending messages that are of interest to our Users. The platform stores the information gathered in a database stored on computer servers in a controlled, secure environment, protected from unauthorized access, use or disclosure. To the extent that third parties gather information, they store that information and are responsible for their use of the same. (As an example, Google is responsible for storing the information its’ Google Analytics gathers from our website or application).</p>
            <p class="text-justify text-4 font-weight-300 mb-5">By using this website, Members hereby confirm that user information including user’s images, banks details, and member’s names will not be posted on any social media which includes but not limited to Facebook, Telegram and Instagram.</p>
            <p class="text-justify text-4 font-weight-300 mb-5">Members also agree that in the event that they divulge the information of fellow members that Bank Alat reserves the right to suspend delete and/or take any action it deems necessary against such a member.</p>
            <p class="text-justify text-4 font-weight-300 mb-5">By using this platform, members agree that they must promote and represent the interest of Bank Alat at all time and if ever you’re found guilty of De-marketing Bank Alat, that Bank Alat reserves the right to take whatever action it deems fit against such members.</p>
            <p class="text-justify text-4 font-weight-300 mb-5">By using this platform, members agree that they must make a testimonial video themselves talking about how Bank Alat have been good to them and how this platform must have changed their lives for the better and they will post the video on there social media accounts. Members agreed that if they refuse to make a testimonial video after receiving from the system, that Bank Alat reserves the right to take whatever action it deems fit against such members including suspending their accounts from be assigned to receive.</p>
            <p class="text-justify text-4 font-weight-300 mb-5">Members also agree that they must not make any donation/payments with money gotten from nefarious activities which is against the law of the country which he/she resides.</p>
            <p class="text-justify text-4 font-weight-300 mb-5">By using this website, Members hereby confirm that they are not child traffickers, prostitutes, Drug pushers, kidnappers, cultists, ritualists, or into any socially unacceptable activities and that whereby he/she under guise participates by becoming a member of Bank Alat, any donation he/she made is seen as charity and neither Bank Alat nor any of her associates will be held accountable. By using this website and facilities provided, Members hereby confirm that they understand that they may experience some delay while trading on the platform either in being assigned a user to pay or receive from.</p>
            <p class="text-justify text-4 font-weight-300 mb-5">The platform may transfer personal information to certain ad partners from whom you have explicitly requested to receive information. It will be clear at the point of collection who is collecting the personal information and whose privacy statement will apply.</p>
            <p class="text-justify text-4 font-weight-300 mb-5">By accessing and/or using this website, Users are entering into a binding agreement with the platform that they will honor any/all pledge(s) they makes and hereby agree that their accounts should be blocked in the events that they fails to honors their pledge when participating in any of her VIP Investment.</p>
            <p class="text-justify text-4 font-weight-300 mb-5">Members hereby agrees that should their accounts be blocked while participating in a Bank Alat programme because of defaults in honoring their pledge by making payment to their assigned participant, that they agree that upon the reopening of their account(s), that they will re-pledge at least double (x2) of their previous pledge before they can have access to any fund they may be having on the system.</p>
            <p class="text-justify text-4 font-weight-300 mb-5">All Bank Alat members are aware and agree that Bank Alat is not a financial institution or a ponzi scheme. Bank Alat is a Bitcoin exchange platform which also supports an active investment programme.</p>
            <p class="text-justify text-4 font-weight-300 mb-5">Members participating in Bank Alat programme understands that the investment programme is participant to participant base and as such should participate only with spare money since donating funds does not necessarily mean receiving donations immediately, hence participants may have to wait longer time until there are more eligible investors in the system.</p>
            <p class="text-justify text-4 font-weight-300 mb-5">By accessing and/or using this website, members hereby agrees that in the event that they wants to terminate their account that they agree to forfeit any/all money in their account(s) and they agree to hold Bank Alat and any/all her promoters including any/all members that they were assigned to pay free from any damages/losses that this decision may result.</p>
            <p class="text-justify text-4 font-weight-300 mb-5">By using this website, Members hereby confirm that they are aware that the Bank Alat is participant to participant base and as such that there shall be no refund of money after payment since all money paid to participants are money due to the participant they paid to. Members hereby agrees that should their accounts be blocked because of defaults in honoring their pledge by making payment to their up-lines they agree to lose any money which may be in their accounts and to also pay any fees which will be determined by the platform in other to gain access to any/all the services provided by the platform.</p>
            <p class="text-justify text-4 font-weight-300 mb-5">Members hereby agrees that the platform reserves the right to block, suspends and deletes their accounts from the platform for what ever reason that the platform deems necessary without prior notice and they also agrees to hold the management of Bank Alat free from any damage/loss that may result as a result of this action.</p>
            <p class="text-justify text-4 font-weight-300 mb-5">Members agree to hold Bank Alat or any of her partners free from any loss which may arise as a result of any attempt to bribe/induce any of her agent in whatever manner in other to divert due process and members understands that Bank Alat has the rights to take whatever action it deems fit against all culprits involved and members hereby agrees to accept in good fate any decision taken.</p>
            <p class="text-justify text-4 font-weight-300 mb-5">Members hereby agrees that any agreements/transactions done outside what is on their dashboard is considered null and void and of no merit and will be considered invalid and whatever outcome it has will not be associated with Bank Alat. </p>
            <p class="text-justify text-4 font-weight-300 mb-5">By accessing and/or using this website, members hereby agrees that they must make an upfront investment minimum of their previous amount invested on the system which will serve as a 100% re-commitment policy which is compulsory for all members before they can make any withdrawal on the system. This means that at all time, members agrees that they must always have an outstanding investment on the system and if they must withdraw it, they must reinvest minimum amount of what they invested initially on the system. </p>
            <p class="text-justify text-4 font-weight-300 mb-5">By accessing and/or using this website, using the provided facilities members hereby agrees that they will participate in Bank Alat programme ONLY with spare money and hereby indemnify the platform of any loss which may occur because of their nonadherence to this clause. </p>
            <p class="text-justify text-4 font-weight-300 mb-5">Members hereby agrees that the security of their passwords is their concern that they will not divulge their personal details to other members but that in the events that they do, they hereby indemnify Bank Alat Management from any loss that may result. </p>
            <p class="text-justify text-4 font-weight-300 mb-5">By accessing and/or using this website, using the provided facilities members hereby agrees that they will not use the same word (thing, name) used for email for password. This is to avoid the persons account from being hacked and the members also confirms that in the event of his/her neglect of this warning he/she will hold Bank Alat and all her associates free from blame. </p>
            <p class="text-justify text-4 font-weight-300 mb-5">By accessing and/or using this website, members hereby agrees that they are to increase the amount they invest on every new investment they make on the platform to maintain high priority in being assigned to receive when they click on withdraw. Members also agrees that Bank Alat reserve the right not to assign anyone to receive if they continuously repeat the same amount on their investments. </p>
            <p class="text-justify text-4 font-weight-300 mb-5">I hereby agrees that by participating on Bank Alat Programme, my priority in receiving should be calculated by the system based on the percentage of increment on my investments and I also agrees that if my priority is low, my chances of been assigned to receive should also be low and I will either wait till it gets to my turn on the queue or upgrade my investment on the system to increase my priority level.</p>
            <p class="text-justify text-4 font-weight-300 mb-5">I hereby agrees that by participating on Bank Alat Programme, my priority in receiving should be calculated by the system based on the percentage of increment on my investments and I also agrees that if my priority is low, my chances of been assigned to receive should also be low and I will either wait till it gets to my turn on the queue or upgrade my investment on the system to increase my priority level.</p>
            <p class="text-justify text-4 font-weight-300 mb-5">By accessing and/or using this website, using the provided facilities members hereby agrees to holds Bank Alat free from any loss which may be occasioned as a result of any cyberattack on our platform.</p>
            <p class="text-justify text-4 font-weight-300 mb-5">Upon termination or expiration of these Terms, your obligations and our proprietary rights, disclaimer of warranties, indemnities, limitations of liability and miscellaneous provisions survive, but your right to use the Bank Alat Website immediately ceases. </p>
            <p class="text-justify text-4 font-weight-300 mb-5">Bank Alat reserves the right, at its sole discretion, to immediately, with or without notice, suspend or terminate these Terms of Use, and/or your access to all or a portion of the Bank Alat Website and/or remove any registration information or User Content from the Bank Alat Website, for any reason (including but not limited to if you breach of any of the provisions of these Terms of Use). Bank Alat also reserves the right to change the Bank Alat Website without notice to you, at any time.</p>
            <p class="text-justify text-4 font-weight-300 mb-5">The platform may also collect information for market research purposes to gain a better understanding of our Members and thus provide more valuable service. The platform may use cookies to store visitors’ preferences and to record session information for purposes including ensuring that visitors are not repeatedly offered the same promotions, to customize newsletter and Web page content based on browser type and User profile information, to help track usage to help us understand which parts of our website are the most popular, where our visitors are going, and how much time they spend there, to make usage of our website even more rewarding as well as to study the effectiveness of our User communications and to customize each visitor’s experience and provide greater convenience.</p>
            <p class="text-justify text-4 font-weight-300 mb-5">From time to time the platform may make changes to its Privacy Policy. If changes are made, they may/may-not be announced to make users aware of what the changes are. Therefore, users are expected to regularly check for any new policy that may have been added/removed from this term. will always be aware of what information may be collected, how it is used, and when it may be disclosed. A User is bound by any changes to the Privacy Policy when she/he uses the site even before/after those changes were/has been posted.</p>
            <p class="text-justify text-4 font-weight-300 mb-5">The present website may contain links to third party websites, where other additional terms and conditions, restrictions, policies and rules may be applicable. Bank Alat is not responsible for the privacy policies and/or practices on other sites or applications. When connecting to or linking to another site, a User should read the privacy policy stated on that site. Our privacy policy only governs information collected on and in relation to the use of the present website.</p>
            <p class="text-justify text-4 font-weight-300 mb-5">Please be aware that whenever you voluntarily post public information to message boards or any other public forum sections available on this website, that information can be accessed by the public and can in turn be used by those people to send you unsolicited communications. The platform is not responsible for the personally identifiable information you choose to submit on the above-mentioned sections. Please use your utmost discretion before you post any personal information online. Users should give special attention to the information they disclose publicly.</p>
            <p class="text-justify text-4 font-weight-300 mb-5">Ads appearing on the present website may be delivered to Users by Bank Alat or third party advertisers. These third parties may set cookies. These cookies allow the ad server to recognize your computer or mobile device each time they send you an online advertisement. In this way, ad servers may compile information about where you, or others who are using your computer or mobile device, spam their advertisements and determine which ads are clicked on. This information allows an ad network to deliver targeted advertisements that they believe will be of most interest to you. This privacy statement covers only the use of cookies by Bank Alat and does not cover the use of cookies by any third-party advertisers.</p>
            <p class="text-justify text-4 font-weight-300 mb-5">Bank Alat is entitled to change these Terms and Conditions at any time. In case of invalidity or incompleteness of any clause of the present Terms and Conditions, the validity of the entire document shall not be affected. Instead of that, the invalid clause shall then be replaced by a valid one whose economic purpose comes as close as possible to that of the invalid clause. The same shall apply when covering a gap requiring regulation.</p>
            <p class="text-justify text-4 font-weight-300 mb-5">By accessing and/or using this website, members hereby gives up their right(s) with regards to any loss their might encounter from the use of this site irrespective of any extent they feel that we contributed to such loss no matter how reasonable or evidence they may have any issue raised in any court of competent jurisdiction you agree that this clause shall be used to determine the outcome of such proceeding. </p>
          </div>
        </div>
      </div>
    </section>
    <!-- Why choose us End --> 
    
    <!-- Special Offer
    ============================================= -->
    <section class="hero-wrap py-5">
      <div class="hero-mask opacity-8 bg-dark"></div>
      <div class="hero-bg" style="background-image:url('<?php echo URLROOT ?>/img/bg/image-2.jpg');"></div>
      <div class="hero-content">
        <div class="container d-md-flex text-center text-md-left align-items-center justify-content-center">
          <h2 class="text-6 font-weight-400 text-white mb-3 mb-md-0">Sign up today and get your first investment</h2>
          <a href="<?php echo URLROOT ?>/pages/register" class="btn btn-outline-light text-nowrap ml-4">Sign up Now</a> </div>
      </div>
    </section>
    <!-- Special Offer end --> 

    
  </div>
  <!-- Content end --> 
  
  <!-- Footer
  ============================================= -->
  <footer id="footer">
    <div class="container">
      <div class="row">
        <div class="col-lg d-lg-flex align-items-center">
          <ul class="nav justify-content-center justify-content-lg-start text-3">
            <li class="nav-item"> <a class="nav-link" href="<?php echo URLROOT; ?> ">Home</a></li>
            <li class="nav-item"> <a class="nav-link" href="<?php echo URLROOT ?>/pages/about">About Us</a></li>
            <li class="nav-item"> <a class="nav-link" href="<?php echo URLROOT ?>/pages/FAQ">FAQ</a></li>
            <li class="nav-item"> <a class="nav-link" href="<?php echo URLROOT ?>/pages/contact">Contact</a></li>
          </ul>
        </div>
        <div class="col-lg d-lg-flex justify-content-lg-end mt-3 mt-lg-0">
          <ul class="social-icons justify-content-center">
            <li class="social-icons-facebook"><a data-toggle="tooltip" href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
            <li class="social-icons-twitter"><a data-toggle="tooltip" href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fab fa-twitter"></i></a></li>
            <li class="social-icons-google"><a data-toggle="tooltip" href="http://www.google.com/" target="_blank" title="Google"><i class="fab fa-whatsapp"></i></a></li>
            <li class="social-icons-youtube"><a data-toggle="tooltip" href="http://www.youtube.com/" target="_blank" title="Youtube"><i class="fab fa-telegram"></i></a></li>
          </ul>
        </div>
      </div>
      <div class="footer-copyright pt-3 pt-lg-2 mt-2">
        <div class="row">
          <div class="col-lg">
            <p class="text-center text-lg-left mb-2 mb-lg-0">Copyright &copy; 2020 <a
                href="https://www.bankalat.com">Bank Alat</a>. All Rights Reserved. <span class="text-success">Build <?php echo VERSION; ?></span>
              </p>
          </div>
          <div class="col-lg d-lg-flex align-items-center justify-content-lg-end">
            <ul class="nav justify-content-center">
              <li class="nav-item"> <a class="nav-link" href="<?php echo URLROOT; ?>/pages/privacy">Terms</a></li>
              <li class="nav-item"> <a class="nav-link" href="<?php echo URLROOT; ?>/pages/privacy">Privacy</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!-- Footer end --> 
  
</div>
<!-- Document Wrapper end --> 

<!-- Back to Top
============================================= --> 
<a id="back-to-top" data-toggle="tooltip" title="Back to Top" href="javascript:void(0)"><i class="fa fa-chevron-up"></i></a> 

<!-- Video Modal
============================================= -->
<div class="modal fade" id="videoModal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content bg-transparent border-0">
      <button type="button" class="close text-white opacity-10 ml-auto mr-n3 font-weight-400" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <div class="modal-body p-0">
        <div class="embed-responsive embed-responsive-16by9">
          <iframe class="embed-responsive-item" id="video" allow="autoplay"></iframe>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Video Modal end --> 
<?php require APPROOT . '/Views/inc/footer.php'; ?>
