<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">
<link href="<?php echo URLROOT; ?>/img/favicon.png" rel="icon" />
<title>Register</title>

<!-- Web Fonts
============================================= -->
<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Rubik:300,300i,400,400i,500,500i,700,700i,900,900i' type='text/css'>

<!-- Stylesheet
============================================= -->
<link rel="stylesheet" type="text/css" href="<?php echo URLROOT; ?>/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo URLROOT; ?>/font-awesome/css/all.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo URLROOT; ?>/css/stylesheet.css" />
</head>
<body>
<!-- Preloader -->
<div id="preloader">
  <div data-loader="dual-ring"></div>
</div>
<!-- Preloader End -->

<div id="main-wrapper" class="h-100">
  <div class="container-fluid px-0 h-100">
    <div class="row no-gutters h-100">
      <div class="col-md-6">
        <!-- Get Verified! Text
        ============================================= -->
        <div class="hero-wrap d-flex align-items-center h-100">
          <div class="hero-mask opacity-8 bg-primary"></div>
          <div class="hero-bg hero-bg-scroll" style="background-image:url('<?php echo URLROOT; ?>/img/bg/naira1.jpg');"></div>
          <div class="hero-content mx-auto w-100 h-100 d-flex flex-column">
            <div class="row  no-gutters">
              <div class="col-10 col-lg-9 mx-auto">
                <div class="logo mt-5 mb-5 mb-md-0"> <a class="d-flex" href="<?php echo URLROOT; ?>"><img src="<?php echo URLROOT; ?>/img/logo-light.png" alt="Bank Alat"></a> </div>
              </div>
            </div>
            <div class="row my-auto">
              <div class="col-10 col-lg-9 mx-auto">
                <h1 class="text-11 text-white mb-4">Invest Now!</h1>
                <p class="text-4 text-white line-height-4 mb-5">Every day, Bank Alat makes thousands of customers happy.</p>
              </div>
            </div>
          </div>
        </div>
        <!-- Get Verified! Text End -->
      </div>
      <div class="col-md-6 d-flex align-items-center">
        <!-- SignUp Form
        ============================================= -->
        <div class="container my-4">
          <div class="row">
            <div class="col-11 col-lg-9 col-xl-8 mx-auto">
              <h3 class="font-weight-400 mb-4">Sign Up</h3>
              <form id="loginForm" method="post" action="<?php echo URLROOT; ?>/pages/register">
                <div class="form-group">
                  <label for="emailPhone">Referral Email Address or Phone Number</label>
                  <input type="text" name="emailPhone" class="form-control <?php echo (!empty($data['emailPhone_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['emailPhone'] ?>" id="emailPhone" placeholder="Enter Your Referral Email or Phone Number">
                  <span class="invalid-feedback"><?php echo $data['emailPhone_err'] ?></span>
                </div>
                <div class="form-group">
                  <label for="emailAddress">Email Address</label>
                  <input type="email" name="email" class="form-control <?php echo (!empty($data['email_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['email'] ?>" id="emailAddress" placeholder="Enter Your Email">
                  <span class="invalid-feedback"><?php echo $data['email_err'] ?></span>
                </div>
                <div class="form-group">
                  <label for="number">Phone Number</label>
                  <input type="number" name="number" class="form-control <?php echo (!empty($data['number_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['number'] ?>" id="emailAddress" placeholder="Phone Number">
                  <span class="invalid-feedback"><?php echo $data['number_err'] ?></span>
                </div>
                <div class="form-group">
                  <label for="loginPassword">Password</label>
                  <input type="password" name="password" class="form-control <?php echo (!empty($data['password_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['password'] ?>" id="loginPassword" placeholder="Password">
                  <span class="invalid-feedback"><?php echo $data['password_err'] ?></span>
                </div>
                <div class="form-group">
                  <label for="confirmPassword">Confirm Password</label>
                  <input type="password" name="confirmPassword" class="form-control <?php echo (!empty($data['confirmPassword_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['confirmPassword'] ?>" id="confirmPassword" placeholder="Confirm Password">
                  <span class="invalid-feedback"><?php echo $data['confirmPassword_err'] ?></span>
                </div>
                <button class="btn btn-primary btn-block my-4" type="submit" name="signup">Sign Up</button>
              </form>
              <p class="text-3 text-center text-muted">Already have an account? <a class="btn-link" href="<?php echo URLROOT; ?>/pages/login">Log In</a></p>
            </div>
          </div>
        </div>
        <!-- SignUp Form End -->
      </div>
    </div>
  </div>
</div>

<!-- Back to Top
============================================= --> 
<a id="back-to-top" data-toggle="tooltip" title="Back to Top" href="javascript:void(0)"><i class="fa fa-chevron-up"></i></a> 

<!-- Script --> 
<script src="<?php echo URLROOT; ?>/js/jquery.min.js"></script> 
<script src="<?php echo URLROOT; ?>/js/bootstrap.bundle.min.js"></script> 
<script src="<?php echo URLROOT; ?>/js/theme.js"></script>
</body>
</html>