<?php require APPROOT . '/Views/inc/header.php'; ?>
<!-- Preloader -->
<div id="preloader">
  <div data-loader="dual-ring"></div>
</div>
<!-- Preloader End -->

<!-- Document Wrapper   
============================================= -->
<div id="main-wrapper">

  <?php require APPROOT . '/Views/inc/nav.php'; ?>

  <!-- Content
  ============================================= -->
  <div id="content">

    <!-- Slideshow
    ============================================= -->
    <div class="owl-carousel owl-theme single-slideshow" data-autoplay="true" data-loop="true" data-autoheight="true"
      data-nav="true" data-items="1">
      <div class="item">
        <section class="hero-wrap section shadow-md">
          <div class="hero-mask opacity-7 bg-dark"></div>
          <div class="hero-bg" style="background-image:url('<?php echo URLROOT ?>/img/bg/naira.jpg');"></div>
          <div class="hero-content py-2 py-lg-5">
            <div class="container text-center">
              <h2 class="text-16 text-white">Invest & Withdraw</h2>
              <p class="text-5 text-white mb-4">Quickly and easily Withdrawer, withdraw and invest online with Bank
                Alat.<br class="d-none d-lg-block"></p>
              <a href="<?php echo URLROOT ?>/pages/register" class="btn btn-primary m-2">Open Account</a>
            </div>
          </div>
        </section>
      </div>
      <div class="item">
        <section class="hero-wrap section shadow-md">
          <div class="hero-mask opacity-7 bg-dark"></div>
          <div class="hero-bg" style="background-image:url('<?php echo URLROOT ?>/img/bg/naira1.jpg');"></div>
          <div class="hero-content py-2 py-lg-5">
            <div class="container">
              <div class="row">
                <div class="col-12 col-lg-8 col-xl-7 text-center text-lg-left">
                  <h2 class="text-13 text-white">Trusted by more than 50,000 people.</h2>
                  <a href="<?php echo URLROOT ?>/pages/register" class="btn btn-primary mr-3">Get started</a>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
    <!-- Slideshow end -->

    <!-- Why choose us
    ============================================= -->
    <section class="section bg-white">
      <div class="container">
        <h2 class="text-9 text-center text-uppercase font-weight-400">Why choose us?</h2>
        <p class="text-4 text-center font-weight-300 mb-5">Here’s Top 4 reasons why investing in Bank Alat.</p>
        <div class="row">
          <div class="col-lg-10 mx-auto">
            <div class="row">
              <div class="col-sm-6 mb-4">
                <div class="featured-box style-3">
                  <div class="featured-box-icon border border-primary text-primary rounded-circle"> <i
                      class="fas fa-history"></i> </div>
                  <h3 class="font-weight-400">24 Hour Support</h3>
                  <p>We gaurantee you a quality support service all round the clock 24/7 every day.</p>
                </div>
              </div>
              <div class="col-sm-6 mb-4">
                <div class="featured-box style-3">
                  <div class="featured-box-icon border border-primary text-primary rounded-circle"> <i
                      class="fas fa-newspaper"></i> </div>
                  <h3 class="font-weight-400">Fast and Efficient pairing</h3>
                  <p>Our matching algorithm is to notch and as extremely fast and effctive to ensure you are paired to donate and receive donation withing 24 hours.</p>
                </div>
              </div>
              <div class="col-sm-6 mb-4 mb-sm-0">
                <div class="featured-box style-3">
                  <div class="featured-box-icon border border-primary text-primary rounded-circle"> <i
                      class="fas fa-credit-card"></i> </div>
                  <h3 class="font-weight-400">Less cash donation</h3>
                  <p>Donate as little amount as N10,000 and get your investment back within 24 hours of donation.</p>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="featured-box style-3">
                  <div class="featured-box-icon border border-primary text-primary rounded-circle"> <i
                      class="fas fa-lock"></i> </div>
                  <h3 class="font-weight-400">100% secure</h3>
                  <p>Our platform is highly secured from cyberbeggers and spammers who ain't ready to make donations.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Why choose us End -->

    <!-- How it works
    ============================================= -->
    <section class="section">
      <div class="container">
        <h2 class="text-9 text-center text-uppercase font-weight-400"> The simple way to invest</h2>
        <!-- <p class="text-4 text-center font-weight-300 mb-5">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p> -->
        <div class="row">
          <div class="col-sm-4 mb-4">
            <div class="featured-box style-4">
              <div class="featured-box-icon text-dark shadow-none border-bottom"><span
                  class="w-100 text-20 font-weight-500">1</span></div>
              <h3 class="mb-3">Sign Up Your Account</h3>
              <p class="text-3 font-weight-300">Sign up for your free Account in few minute.</p>
            </div>
          </div>
          <div class="col-sm-4 mb-4">
            <div class="featured-box style-4">
              <div class="featured-box-icon text-dark shadow-none border-bottom"><span
                  class="w-100 text-20 font-weight-500">2</span></div>
              <h3 class="mb-3">Invest</h3>
              <p class="text-3 font-weight-300">Invest into Bank Alat.</p>
            </div>
          </div>
          <div class="col-sm-4 mb-4 mb-sm-0">
            <div class="featured-box style-4">
              <div class="featured-box-icon text-dark shadow-none border-bottom"><span
                  class="w-100 text-20 font-weight-500">3</span></div>
              <h3 class="mb-3">Withdraw Funds</h3>
              <p class="text-3 font-weight-300">Your funds will be transferred to your local bank account.</p>
            </div>
          </div>
        </div>
        <div class="text-center mt-2"><a href="<?php echo URLROOT ?>/pages/register" class="btn btn-primary">Open a Free
            Account</a></div>
      </div>
    </section>

    <section class="section bg-light">
      <div class="container">
        <div class="row">
          <div class="col-lg-4">
            <div class="card text-center">
              <div class="card-header">
                Basic
              </div>
              <div class="card-body">
                <h5 class="card-title border-bottom">&#8358 10,000</h5>
                <h5 class="card-title border-bottom">Auto Matching</h5>
                <h5 class="card-title border-bottom">50% ROI</h5>
                <p class="card-text">Faster the payment, faster your withdrawer</p>
                <a href="#" class="btn btn-primary">Invest</a>
              </div>
              <div class="card-footer text-muted">
                3 days ago
              </div>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="card text-center">
              <div class="card-header">
                Silver
              </div>
              <div class="card-body">
                <h5 class="card-title border-bottom">&#8358 20,000</h5>
                <h5 class="card-title border-bottom">Auto Matching</h5>
                <h5 class="card-title border-bottom">50% ROI</h5>
                <p class="card-text">Faster the payment, faster your withdrawer</p>
                <a href="#" class="btn btn-primary">Invest</a>
              </div>
              <div class="card-footer text-muted">
                3 days ago
              </div>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="card text-center">
              <div class="card-header">
                Gold
              </div>
              <div class="card-body">
                <h5 class="card-title border-bottom">&#8358 50,000</h5>
                <h5 class="card-title border-bottom">Auto Matching</h5>
                <h5 class="card-title border-bottom">50% ROI</h5>
                <p class="card-text">Faster the payment, faster your withdrawer</p>
                <a href="#" class="btn btn-primary">Invest</a>
              </div>
              <div class="card-footer text-muted">
                3 days ago
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Testimonial
    ============================================= -->
    <section class="section">
      <div class="container">
        <h2 class="text-9 text-center">What people are saying about Bank Alat</h2>
        <p class="text-4 text-center mb-4">A paying platform people love to talk about</p>
        <div class="owl-carousel owl-theme" data-autoplay="true" data-nav="true" data-loop="true" data-margin="30"
          data-slideby="2" data-stagepadding="5" data-items-xs="1" data-items-sm="1" data-items-md="2"
          data-items-lg="2">
          <div class="item">
            <div class="testimonial rounded text-center p-4">
              <p class="text-4">“Easy to use, reasonably priced simply dummy text of the printing and typesetting
                industry. Quidam lisque persius interesset his et, in quot quidam possim iriure.”</p>
              <strong class="d-block font-weight-500">Jay Shah</strong> <span class="text-muted">Founder at Icomatic Pvt
                Ltd</span>
            </div>
          </div>
          <div class="item">
            <div class="testimonial rounded text-center p-4">
              <p class="text-4">“I am happy Working with printing and typesetting industry. Quidam lisque persius
                interesset his et, in quot quidam persequeris essent possim iriure.”</p>
              <strong class="d-block font-weight-500">Patrick Cary</strong> <span class="text-muted">Freelancer from
                USA</span>
            </div>
          </div>
          <div class="item">
            <div class="testimonial rounded text-center p-4">
              <p class="text-4">“Fast easy to use transfers to a different currency. Much better value that the banks.”
              </p>
              <strong class="d-block font-weight-500">De Mortel</strong> <span class="text-muted">Online Retail</span>
            </div>
          </div>
          <div class="item">
            <div class="testimonial rounded text-center p-4">
              <p class="text-4">“I have used them twice now. Good rates, very efficient service and it denies high
                street banks an undeserved windfall. Excellent.”</p>
              <strong class="d-block font-weight-500">Chris Tom</strong> <span class="text-muted">User from UK</span>
            </div>
          </div>
          <div class="item">
            <div class="testimonial rounded text-center p-4">
              <p class="text-4">“It's a real good idea to manage your money by payyed. The rates are fair and you can
                carry out the transactions without worrying!”</p>
              <strong class="d-block font-weight-500">Mauri Lindberg</strong> <span class="text-muted">Freelancer from
                Australia</span>
            </div>
          </div>
          <div class="item">
            <div class="testimonial rounded text-center p-4">
              <p class="text-4">“Only trying it out since a few days. But up to now excellent. Seems to work flawlessly.
                I'm only using it for sending money to friends at the moment.”</p>
              <strong class="d-block font-weight-500">Dennis Jacques</strong> <span class="text-muted">User from
                USA</span>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Testimonial end -->

    <!-- Frequently asked questions
    ============================================= -->
    <section class="section bg-white">
      <div class="container">
        <h2 class="text-9 text-center">Frequently Asked Questions</h2>
        <p class="text-4 text-center mb-4 mb-sm-5">Can't find it here? Check out our</p>
        <div class="row">
          <div class="col-md-10 col-lg-8 mx-auto">
            <hr class="mb-0">
            <div class="accordion accordion-alternate arrow-right" id="popularTopics">
              <div class="card">
                <div class="card-header" id="heading1">
                  <h5 class="mb-0"> <a href="#" class="collapsed" data-toggle="collapse" data-target="#collapse1"
                      aria-expanded="false" aria-controls="collapse1">What is Bank Alat?</a> </h5>
                </div>
                <div id="collapse1" class="collapse" aria-labelledby="heading1" data-parent="#popularTopics">
                  <div class="card-body"> Bank Alat is an investment platform which offers leveraged margin investment
                    programme through a peer-to-peer funding market using Nigeria Naira currency. Bank Alat has
                    developed a National coverage, multiple payment options and very active support which are
                    accompanied by time-proven platform stability that guarantees safety of assets, funds and data.
                  </div>
                </div>
              </div>
              <div class="card">
                <div class="card-header" id="heading2">
                  <h5 class="mb-0"> <a href="#" class="collapsed" data-toggle="collapse" data-target="#collapse2"
                      aria-expanded="false" aria-controls="collapse2">Who is eligible to participate?</a> </h5>
                </div>
                <div id="collapse2" class="collapse" aria-labelledby="heading2" data-parent="#popularTopics">
                  <div class="card-body"> Only Users that have activated their account are eligible to participate in
                    the Bank Alat Programme. However you must be 18 years or about to participate.</div>
                </div>
              </div>
              <div class="card">
                <div class="card-header" id="heading3">
                  <h5 class="mb-0"> <a href="#" class="collapsed" data-toggle="collapse" data-target="#collapse3"
                      aria-expanded="false" aria-controls="collapse3">What is the minimum I can invest?</a> </h5>
                </div>
                <div id="collapse3" class="collapse" aria-labelledby="heading3" data-parent="#popularTopics">
                  <div class="card-body"> The minimum you can invest in Naira is ₦10,000.00.</div>
                </div>
              </div>
              <div class="card">
                <div class="card-header" id="heading4">
                  <h5 class="mb-0"> <a href="#" class="collapsed" data-toggle="collapse" data-target="#collapse4"
                      aria-expanded="false" aria-controls="collapse4">Can I Increase the amount I'm Investing?</a> </h5>
                </div>
                <div id="collapse4" class="collapse" aria-labelledby="heading4" data-parent="#popularTopics">
                  <div class="card-body"> Yes, you can choose to increase the amount you are invested any time but you
                    will not be able to reduce your amount after increasing it. </div>
                </div>
              </div>
              <div class="card">
                <div class="card-header" id="heading5">
                  <h5 class="mb-0"> <a href="#" class="collapsed" data-toggle="collapse" data-target="#collapse5"
                      aria-expanded="false" aria-controls="collapse5">Can I Invest with money I am expecting?</a> </h5>
                </div>
                <div id="collapse5" class="collapse" aria-labelledby="heading5" data-parent="#popularTopics">
                  <div class="card-body"> NO, we strongly kick against members investing amount based on the money they
                    are expecting. This is because if a participant that is assigned to pay you defaults your payment,
                    you automatically defaults the payment of the participant you're assigned to Pay. This will
                    ultimately lead to the blocking of your account since we do not tolerate default irrespective of how
                    genuine your reason(s) might be.</div>
                </div>
              </div>
              <div class="card">
                <div class="card-header" id="heading6">
                  <h5 class="mb-0"> <a href="#" class="collapsed" data-toggle="collapse" data-target="#collapse6"
                      aria-expanded="false" aria-controls="collapse6">Can I borrow Money to participate?</a> </h5>
                </div>
                <div id="collapse6" class="collapse" aria-labelledby="heading6" data-parent="#popularTopics">
                  <div class="card-body"> We strongly object to Members borrowing money to participate. Members are
                    rather encouraged to participate ONLY with THEIR SPARE MONEY. This is because all Investments on the
                    system are considered as pure DONATIONS to each other.</div>
                </div>
              </div>
            </div>
            <hr class="mt-0">
          </div>
        </div>
        <div class="text-center mt-4"><a href="<?php echo URLROOT ?>/pages/FAQ" class="btn-link text-4">See more FAQ<i
              class="fas fa-chevron-right text-2 ml-2"></i></a></div>
      </div>
    </section>
    <!-- Frequently asked questions end -->

    <!-- JOIN US
    ============================================= -->
    <section class="section py-5">
      <div class="container text-center">
        <div class="row justify-content-center">
          <div class="col-sm-6 col-md-3 mt-4 mt-md-0">
            <div class="featured-box text-center">
              <div class="featured-box-icon mb-2"> <i class="fas fa-users"></i> </div>
              <h4 class="text-12 mb-0">2.5M</h4>
              <p class="text-4 mb-0">Users</p>
            </div>
          </div>
          <div class="col-sm-6 col-md-3 mt-4 mt-md-0">
            <div class="featured-box text-center">
              <div class="featured-box-icon mb-2"> <i class="far fa-life-ring"></i> </div>
              <h4 class="text-12 mb-0">24/7</h4>
              <p class="text-4 mb-0">Support</p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- JOIN US end -->

    <!-- Special Offer
    ============================================= -->
    <section class="hero-wrap py-5">
      <div class="hero-mask opacity-8 bg-dark"></div>
      <div class="hero-bg" style="background-image:url('<?php echo URLROOT ?>/img/bg/image-2.jpg');"></div>
      <div class="hero-content">
        <div class="container d-md-flex text-center text-md-left align-items-center justify-content-center">
          <h2 class="text-6 font-weight-400 text-white mb-3 mb-md-0">Sign up today and get your first investment</h2>
          <a href="<?php echo URLROOT ?>/pages/register" class="btn btn-outline-light text-nowrap ml-4">Sign up Now</a>
        </div>
      </div>
    </section>
    <!-- Special Offer end -->


  </div>
  <!-- Content end -->

  <!-- Footer
  ============================================= -->
  <footer id="footer">
    <div class="container">
      <div class="row">
        <div class="col-lg d-lg-flex align-items-center">
          <ul class="nav justify-content-center justify-content-lg-start text-3">
            <li class="nav-item"> <a class="nav-link" href="<?php echo URLROOT; ?> ">Home</a></li>
            <li class="nav-item"> <a class="nav-link" href="<?php echo URLROOT ?>/pages/about">About Us</a></li>
            <li class="nav-item"> <a class="nav-link" href="<?php echo URLROOT ?>/pages/FAQ">FAQ</a></li>
            <li class="nav-item"> <a class="nav-link" href="<?php echo URLROOT ?>/pages/contact">Contact</a></li>
          </ul>
        </div>
        <div class="col-lg d-lg-flex justify-content-lg-end mt-3 mt-lg-0">
          <ul class="social-icons justify-content-center">
            <li class="social-icons-facebook"><a data-toggle="tooltip" href="http://www.facebook.com/" target="_blank"
                title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
            <li class="social-icons-twitter"><a data-toggle="tooltip" href="http://www.twitter.com/" target="_blank"
                title="Twitter"><i class="fab fa-twitter"></i></a></li>
            <li class="social-icons-google"><a data-toggle="tooltip" href="http://www.google.com/" target="_blank"
                title="Google"><i class="fab fa-whatsapp"></i></a></li>
            <li class="social-icons-youtube"><a data-toggle="tooltip" href="http://www.youtube.com/" target="_blank"
                title="Youtube"><i class="fab fa-telegram"></i></a></li>
          </ul>
        </div>
      </div>
      <div class="footer-copyright pt-3 pt-lg-2 mt-2">
        <div class="row">
          <div class="col-lg">
            <p class="text-center text-lg-left mb-2 mb-lg-0">Copyright &copy; 2020 <a
                href="https://www.bankalat.com">Bank Alat</a>. All Rights Reserved. <span class="text-success">Build <?php echo VERSION; ?></span>
              </p>
          </div>
          <div class="col-lg d-lg-flex align-items-center justify-content-lg-end">
            <ul class="nav justify-content-center">
              <li class="nav-item"> <a class="nav-link" href="<?php echo URLROOT; ?>/pages/privacy">Terms</a></li>
              <li class="nav-item"> <a class="nav-link" href="<?php echo URLROOT; ?>/pages/privacy">Privacy</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!-- Footer end -->

</div>
<!-- Document Wrapper end -->

<!-- Back to Top
============================================= -->
<a id="back-to-top" data-toggle="tooltip" title="Back to Top" href="javascript:void(0)"><i
    class="fa fa-chevron-up"></i></a>

<!-- Video Modal
============================================= -->
<div class="modal fade" id="videoModal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content bg-transparent border-0">
      <button type="button" class="close text-white opacity-10 ml-auto mr-n3 font-weight-400" data-dismiss="modal"
        aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <div class="modal-body p-0">
        <div class="embed-responsive embed-responsive-16by9">
          <iframe class="embed-responsive-item" id="video" allow="autoplay"></iframe>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Video Modal end -->
<?php require APPROOT . '/Views/inc/footer.php'; ?>
