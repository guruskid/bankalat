<?php require APPROOT . '/Views/inc/header.php'; ?>
<!-- Preloader -->
<div id="preloader">
  <div data-loader="dual-ring"></div>
</div>
<!-- Preloader End --> 

<!-- Document Wrapper   
============================================= -->
<div id="main-wrapper">
  
  <?php require APPROOT . '/Views/inc/nav.php'; ?>

  <!-- Page Header
============================================= -->
<section class="page-header page-header-text-light bg-dark-3 py-5">
  <div class="container">
    <div class="row text-center">
      <div class="col-12">
        <ul class="breadcrumb mb-0">
          <li><a href="<?php echo URLROOT ?>">Home</a></li>
          <li class="active">FAQ</li>
        </ul>
      </div>
      <div class="col-12">
        <h1>FAQ</h1>
      </div>
    </div>
  </div>
</section>
<!-- Page Header End --> 
  
  <!-- Content
  ============================================= -->
  <div id="content">
  
  <!-- Main Topics
    ============================================= -->
    <section class="section py-3 my-3 py-sm-5 my-sm-5">
      <div class="container">
        <div class="row">
          <div class="col-sm-6 col-lg-3 mb-4 mb-lg-0">
            <div class="bg-light shadow-sm rounded p-4 text-center"> <span class="d-block text-17 text-primary mt-2 mb-3"><i class="fas fa-user-circle"></i></span>
              <h3 class="text-body text-4">My Account</h3>
              <!-- <p class="mb-0"><a class="text-muted btn-link" href="">See articles<span class="text-1 ml-1"><i class="fas fa-chevron-right"></i></span></a></p> -->
            </div>
          </div>
          <div class="col-sm-6 col-lg-3 mb-4 mb-lg-0">
            <div class="bg-light shadow-sm rounded p-4 text-center"> <span class="d-block text-17 text-primary mt-2 mb-3"><i class="fas fa-money-check-alt"></i></span>
              <h3 class="text-body text-4">Payment</h3>
              <!-- <p class="mb-0"><a class="text-muted btn-link" href="">See articles<span class="text-1 ml-1"><i class="fas fa-chevron-right"></i></span></a></p> -->
            </div>
          </div>
          <div class="col-sm-6 col-lg-3 mb-4 mb-sm-0">
            <div class="bg-light shadow-sm rounded p-4 text-center"> <span class="d-block text-17 text-primary mt-2 mb-3"><i class="fas fa-shield-alt"></i></span>
              <h3 class="text-body text-4">Security</h3>
              <!-- <p class="mb-0"><a class="text-muted btn-link" href="">See articles<span class="text-1 ml-1"><i class="fas fa-chevron-right"></i></span></a></p> -->
            </div>
          </div>
          <div class="col-sm-6 col-lg-3">
            <div class="bg-light shadow-sm rounded p-4 text-center"> <span class="d-block text-17 text-primary mt-2 mb-3"><i class="fas fa-credit-card"></i></span>
              <h3 class="text-body text-4">Payment Methods</h3>
              <!-- <p class="mb-0"><a class="text-muted btn-link" href="">See articles<span class="text-1 ml-1"><i class="fas fa-chevron-right"></i></span></a></p> -->
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Main Topics end -->

    <!-- Popular Topics
    ============================================= -->
    <section class="section bg-white">
      <div class="container">
        <h2 class="text-9 text-center">Popular Topics</h2>
        <p class="text-4 text-center mb-5"></p>
        <div class="row">
          <div class="col-md-10 mx-auto">
            <div class="row">
              <div class="col-md-6">
                <div class="accordion accordion-alternate" id="popularTopics">
                  <div class="card">
                    <div class="card-header" id="heading1">
                      <h5 class="mb-0"> <a href="#" class="collapsed" data-toggle="collapse" data-target="#collapse1" aria-expanded="false" aria-controls="collapse1">What is Bank Alat?</a> </h5>
                    </div>
                    <div id="collapse1" class="collapse" aria-labelledby="heading1" data-parent="#popularTopics">
                      <div class="card-body"> Bank Alat is an investment platform which offers leveraged margin investment programme through a peer-to-peer funding market using Nigeria Naira currency. Bank Alat has developed a National coverage, multiple payment options and very active support which are accompanied by time-proven platform stability that guarantees safety of assets, funds and data. </div>
                    </div>
                  </div>
                  <div class="card">
                    <div class="card-header" id="heading2">
                      <h5 class="mb-0"> <a href="#" class="collapsed" data-toggle="collapse" data-target="#collapse2" aria-expanded="false" aria-controls="collapse2">Who is eligible to participate?</a> </h5>
                    </div>
                    <div id="collapse2" class="collapse" aria-labelledby="heading2" data-parent="#popularTopics">
                      <div class="card-body">  Only Users that have activated their account are eligible to participate in the Bank Alat Programme. However you must be 18 years or about to participate. </div>
                    </div>
                  </div>
                  <div class="card">
                    <div class="card-header" id="heading3">
                      <h5 class="mb-0"> <a href="#" class="collapsed" data-toggle="collapse" data-target="#collapse3" aria-expanded="false" aria-controls="collapse3">What is the minimum I can invest?</a> </h5>
                    </div>
                    <div id="collapse3" class="collapse" aria-labelledby="heading3" data-parent="#popularTopics">
                      <div class="card-body"> The minimum you can invest in Naira is ₦10,000.00. </div>
                    </div>
                  </div>
                  <div class="card">
                    <div class="card-header" id="heading4">
                      <h5 class="mb-0"> <a href="#" class="collapsed" data-toggle="collapse" data-target="#collapse4" aria-expanded="false" aria-controls="collapse4">Can I Increase the amount I'm Investing?</a> </h5>
                    </div>
                    <div id="collapse4" class="collapse" aria-labelledby="heading4" data-parent="#popularTopics">
                      <div class="card-body"> Yes, you can choose to increase the amount you are invested any time but you will not be able to reduce your amount after increasing it. </div>
                    </div>
                  </div>
                  <div class="card">
                    <div class="card-header" id="heading5">
                      <h5 class="mb-0"> <a href="#" class="collapsed" data-toggle="collapse" data-target="#collapse5" aria-expanded="false" aria-controls="collapse5">Can I Invest with money I am expecting?</a> </h5>
                    </div>
                    <div id="collapse5" class="collapse" aria-labelledby="heading5" data-parent="#popularTopics">
                      <div class="card-body"> NO, we strongly kick against members investing amount based on the money they are expecting. This is because if a participant that is assigned to pay you defaults your payment, you automatically defaults the payment of the participant you're assigned to Pay. This will ultimately lead to the blocking of your account since we do not tolerate default irrespective of how genuine your reason(s) might be. </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="accordion accordion-alternate" id="popularTopics2">
                  <div class="card">
                    <div class="card-header" id="heading6">
                      <h5 class="mb-0"> <a href="#" class="collapsed" data-toggle="collapse" data-target="#collapse6" aria-expanded="false" aria-controls="collapse6">Can I borrow Money to participate?</a> </h5>
                    </div>
                    <div id="collapse6" class="collapse" aria-labelledby="heading6" data-parent="#popularTopics2">
                      <div class="card-body"> We strongly object to Members borrowing money to participate. Members are rather encouraged to participate ONLY with THEIR SPARE MONEY. This is because all Investments on the system are considered as pure DONATIONS to each other. </div>
                    </div>
                  </div>
                  <div class="card">
                    <div class="card-header" id="heading7">
                      <h5 class="mb-0"> <a href="#" class="collapsed" data-toggle="collapse" data-target="#collapse7" aria-expanded="false" aria-controls="collapse7">Who is a Regular User?</a> </h5>
                    </div>
                    <div id="collapse7" class="collapse" aria-labelledby="heading7" data-parent="#popularTopics2">
                      <div class="card-body"> A Regular user is a registered member of Bnk Alat. </div>
                    </div>
                  </div>
                  <div class="card">
                    <div class="card-header" id="heading8">
                      <h5 class="mb-0"> <a href="#" class="collapsed" data-toggle="collapse" data-target="#collapse8" aria-expanded="false" aria-controls="collapse8">Who is a Guider?</a> </h5>
                    </div>
                    <div id="collapse8" class="collapse" aria-labelledby="heading8" data-parent="#popularTopics2">
                      <div class="card-body"> A Guider is a Regular user of Bank Alat who have reffered up to 10 actived users on the system. As a Guider he/she will be earning extra percentage from every users under them. </div>
                    </div>
                  </div>
                  <div class="card">
                    <div class="card-header" id="heading9">
                      <h5 class="mb-0"> <a href="#" class="collapsed" data-toggle="collapse" data-target="#collapse9" aria-expanded="false" aria-controls="collapse9">Do I have to refer someone to earn? </a> </h5>
                    </div>
                    <div id="collapse9" class="collapse" aria-labelledby="heading9" data-parent="#popularTopics2">
                      <div class="card-body">You are not mandated to refer someone to earn but you can choose to refer a friend and earn 10% of your referred users first Investments. There are no limits to this. </div>
                    </div>
                  </div>
                  <div class="card">
                    <div class="card-header" id="heading10">
                      <h5 class="mb-0"> <a href="#" class="collapsed" data-toggle="collapse" data-target="#collapse10" aria-expanded="false" aria-controls="collapse10">How do I Receive My Profit after Investing?</a> </h5>
                    </div>
                    <div id="collapse10" class="collapse" aria-labelledby="heading10" data-parent="#popularTopics2">
                      <div class="card-body"> You are eligible to receive payment from your Investment once you have fulfilled the necessary conditions for it, then you can withdraw by clicking on the withdraw button on your dashboard when the Investment is due for withdrawal. You will be merged to a User(s) that will to pay you and the money will be sent to the bank account you registered with. </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- <div class="text-center mt-4"><a href="#" class="btn-link text-4">See more topics<i class="fas fa-chevron-right text-2 ml-2"></i></a></div> -->
      </div>
    </section>
    <!-- Popular Topics end -->

    <!-- Can't find
    ============================================= -->
    <section class="section py-4 my-4 py-sm-5 my-sm-5">
      <div class="container">
        <div class="row">
          <div class="col-lg-6">
            <div class="bg-white shadow-sm rounded pl-4 pl-sm-0 pr-4 py-4">
              <div class="row no-gutters">
                <div class="col-12 col-sm-auto text-13 text-light d-flex align-items-center justify-content-center"> <span class="px-4 ml-3 mr-2 mb-4 mb-sm-0"><i class="far fa-envelope"></i></span> </div>
                <div class="col text-center text-sm-left">
                  <div class="">
                    <h5 class="text-3 text-body">Can't find what you're looking for?</h5>
                    <p class="text-muted mb-0">We want to answer all of your queries. Get in touch and we'll get back to you as soon as we can. <a class="btn-link" href="<?php echo URLROOT; ?>/pages/contact">Contact us<span class="text-1 ml-1"><i class="fas fa-chevron-right"></i></span></a></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-6 mt-4 mt-lg-0">
            <div class="bg-white shadow-sm rounded pl-4 pl-sm-0 pr-4 py-4">
              <div class="row no-gutters">
                <div class="col-12 col-sm-auto text-13 text-light d-flex align-items-center justify-content-center"> <span class="px-4 ml-3 mr-2 mb-4 mb-sm-0"><i class="far fa-comment-alt"></i></span> </div>
                <div class="col text-center text-sm-left">
                  <div class="">
                    <h5 class="text-3 text-body">Technical questions</h5>
                    <p class="text-muted mb-0">Have some technical questions? Hit us up on live chat or whatever. <a class="btn-link" href="">Click here<span class="text-1 ml-1"><i class="fas fa-chevron-right"></i></span></a></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Can't find end -->
  <!-- Content end -->
  
  <!-- Footer
  ============================================= -->
  <footer id="footer">
    <div class="container">
      <div class="row">
        <div class="col-lg d-lg-flex align-items-center">
          <ul class="nav justify-content-center justify-content-lg-start text-3">
            <li class="nav-item"> <a class="nav-link" href="<?php echo URLROOT; ?> ">Home</a></li>
            <li class="nav-item"> <a class="nav-link" href="<?php echo URLROOT ?>/pages/about">About Us</a></li>
            <li class="nav-item"> <a class="nav-link" href="<?php echo URLROOT ?>/pages/FAQ">FAQ</a></li>
            <li class="nav-item"> <a class="nav-link" href="<?php echo URLROOT ?>/pages/contact">Contact</a></li>
          </ul>
        </div>
        <div class="col-lg d-lg-flex justify-content-lg-end mt-3 mt-lg-0">
          <ul class="social-icons justify-content-center">
            <li class="social-icons-facebook"><a data-toggle="tooltip" href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
            <li class="social-icons-twitter"><a data-toggle="tooltip" href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fab fa-twitter"></i></a></li>
            <li class="social-icons-google"><a data-toggle="tooltip" href="http://www.google.com/" target="_blank" title="Google"><i class="fab fa-whatsapp"></i></a></li>
            <li class="social-icons-youtube"><a data-toggle="tooltip" href="http://www.youtube.com/" target="_blank" title="Youtube"><i class="fab fa-telegram"></i></a></li>
          </ul>
        </div>
      </div>
      <div class="footer-copyright pt-3 pt-lg-2 mt-2">
        <div class="row">
          <div class="col-lg">
            <p class="text-center text-lg-left mb-2 mb-lg-0">Copyright &copy; 2020 <a
                href="https://www.bankalat.com">Bank Alat</a>. All Rights Reserved. <span class="text-success">Build <?php echo VERSION; ?></span>
              </p>
          </div>
          <div class="col-lg d-lg-flex align-items-center justify-content-lg-end">
            <ul class="nav justify-content-center">
              <li class="nav-item"> <a class="nav-link" href="<?php echo URLROOT; ?>/pages/privacy">Terms</a></li>
              <li class="nav-item"> <a class="nav-link" href="<?php echo URLROOT; ?>/pages/privacy">Privacy</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!-- Footer end --> 
  
</div>
<!-- Document Wrapper end --> 

<!-- Back to Top
============================================= --> 
<a id="back-to-top" data-toggle="tooltip" title="Back to Top" href="javascript:void(0)"><i class="fa fa-chevron-up"></i></a> 

<!-- Video Modal
============================================= -->
<div class="modal fade" id="videoModal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content bg-transparent border-0">
      <button type="button" class="close text-white opacity-10 ml-auto mr-n3 font-weight-400" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <div class="modal-body p-0">
        <div class="embed-responsive embed-responsive-16by9">
          <iframe class="embed-responsive-item" id="video" allow="autoplay"></iframe>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Video Modal end --> 
<?php require APPROOT . '/Views/inc/footer.php'; ?>
