<?php 
class Users extends Controller {
    public function __construct() {

    }

    public function index() {
        $data = [
            'title' => 'Bank Alat'
        ];
        $this -> view('users/index', $data);
    }

    public function testimony() {
        $data = [
            'title' => 'Bank Ala'
        ];
        $this -> view('users/testimony');
    }

    public function activate() {
        $data = [
            'title' => 'Bank Alat'
        ];
        $this -> view('users/activate');
    }
}